#include "GSMModem.h"

namespace RFGSM {

GSMModem::GSMModem() : handle(nullptr) {

}

GSMModem::~GSMModem() {
	if (handle != nullptr) {
		GSMClose();
	}
}

bool GSMModem::GSMOpen() {
	if (handle != nullptr)
		GSMClose();
	handle = new serial::Serial();
	if (handle == nullptr)
		return false;
	try {
		handle->setBaudrate(115200);
		handle->setTimeout(10, 1, 1, 250, 0);
		handle->setPort("/dev/ttyACM3");
		handle->open();
		if (handle->isOpen()) {
			handle->write(std::string("ATE0;#SLED=2,1,1;#GPIO=1,0,2\r\n"));
			usleep(250000);
			uint8_t buffer[100];
			while (handle->read(buffer, 100) > 0);
			sleep(1);
			return true;
		}
	}
	catch(...) {

	}
	return false;
}

bool GSMModem::GSMClose() {
	if (handle == nullptr)
		return false;
	if (handle->isOpen())
		handle->close();
	handle->~Serial();
	handle = nullptr;
	return true;
}

bool GSMModem::GSMModemExists() {
	try {
		std::string res = ExecuteShellCmd("ls /dev/ttyACM* | grep -c ACM[0,3]");
		return !res.empty() && atoi(res.c_str()) > 0;
	}
	catch (...) {

	}
	return false;
}

bool GSMModem::SendATCommand(const char *commandString, bool terminate, char *responseString) {
	size_t noOfBytesRead = 0;
	size_t timeOut = 50;
	uint8_t buffer[100];
	std::string cmd = std::string(commandString);
	if (terminate)
		cmd += std::string("\r\n");
	handle->write(cmd);

	if (responseString == NULL)
		return true;
	while (timeOut) // If response to command is expected
	{
		noOfBytesRead = handle->read(buffer, 100);
		if (noOfBytesRead > 0 && strstr((char*)buffer, responseString))
			return true;
		else
			usleep(50000);
		timeOut--;
	}
	return false;
}

bool GSMModem::SendATCommand(const char *commandString, bool terminate, int timeout, char *responseString) {
	size_t noOfBytesRead = 0;
	size_t totalBytesRead = 0;
	uint8_t buffer[100];
	std::string cmd = std::string(commandString);
	if (terminate)
		cmd += std::string("\r\n");
	handle->write(cmd);

	if (responseString == NULL)
		return true;
	while (timeout) // If response to command is expected
	{
		noOfBytesRead = handle->read(buffer, 100);
		if (noOfBytesRead > 0) { // && strstr((char*)buffer, responseString))
			strncpy(responseString + totalBytesRead, (char*)buffer, noOfBytesRead);
			totalBytesRead += noOfBytesRead;
		}
		else
			usleep(50000);
		timeout--;
	}
	return totalBytesRead > 0;
}

bool GSMModem::GSMGetIMEINumber(char *imeiNumber) {
	size_t bytesTransmitted = 0;
	size_t noOfBytesRead = 0;
	uint8_t buffer[32];
	bytesTransmitted = handle->write(std::string("AT+CGSN\r\n"));
	if (bytesTransmitted != 9)
		return false;
	usleep(250000);
	noOfBytesRead = handle->read(buffer, 32);
	if (noOfBytesRead >= IMEI_NUMBER_MAX_LENGTH + 2) {
		strncpy(imeiNumber, (char*)(buffer + 2), IMEI_NUMBER_MAX_LENGTH);
		return true;
	}
	return false;
}

bool GSMModem::GsmGetSignalStrength(char *signalStrength) {
	size_t bytesTransmitted = 0;
	size_t noOfBytesRead = 0;
	char buffer[100] = {0};
	char *start = NULL;
	char *end = NULL;

	bytesTransmitted = handle->write(std::string("AT+CSQ\r\n"));
	if (bytesTransmitted != 8)
		return false;
	usleep(250000);
	noOfBytesRead = handle->read((unsigned char*)buffer, 100);
	if (noOfBytesRead == 0)
		return false;

	start = strstr(buffer, ": ");
	end = strstr(start + 1, ",");
	if (!start || !end)
		return false;
	strncat(signalStrength, start + 2, end - (start + 2));
	return true;
}

bool GSMModem::GsmGetSignalStrengthDMB(int *signalStrength) {
	char str[4] = {0};
	if (!GsmGetSignalStrength(str))
		return false;
	int status;
	std::stringstream ss;
	ss << std::dec << std::string(str);
	ss >> status;

	if (status == 99)
		return false;

	*signalStrength = -113 + (status * 2);

	return true;
}

bool GSMModem::GsmGetManufacturerInfo(char *manufacturer, char *modelNumber) {
	size_t bytesTransmitted = 0;
	size_t noOfBytesRead = 0;
	char buffer[100] = {0};
	while (true) {
		noOfBytesRead = handle->read((unsigned char*)buffer, 100);
		memset(buffer, 0, 100);
		if (noOfBytesRead == 0)
			break;
	}

	bytesTransmitted = handle->write(std::string("AT+CGMI\r\n"));
	if (bytesTransmitted != 9)
		return false;
	usleep(250000);
	noOfBytesRead = handle->read((unsigned char*)buffer, 100);
	if (noOfBytesRead == 0)
		return false;

	strncat(manufacturer, buffer + 2, strlen(buffer) - 10);

	bytesTransmitted = handle->write(std::string("AT+CGMM\r\n"));
	if (bytesTransmitted != 9)
		return false;
	usleep(250000);
	noOfBytesRead = handle->read((unsigned char*)buffer, 100);
	if (noOfBytesRead == 0)
		return false;

	strncat(modelNumber, buffer + 2, strlen(buffer) - 10);
	return true;
}

bool GSMModem::GsmGetSWVersion(char *version) {
	size_t bytesTransmitted = 0;
	size_t noOfBytesRead = 0;
	char buffer[100] = {0};
	while (true) {
		noOfBytesRead = handle->read((unsigned char*)buffer, 100);
		memset(buffer, 0, 100);
		if (noOfBytesRead == 0)
			break;
	}

	bytesTransmitted = handle->write(std::string("AT+CGMR\r\n"));
	if (bytesTransmitted != 9)
		return false;
	usleep(250000);
	noOfBytesRead = handle->read((unsigned char*)buffer, 100);
	if (noOfBytesRead == 0)
		return false;

	strncat(version, buffer + 2, strlen(buffer) - 10);
	return true;
}

bool GSMModem::GsmGetSIMStaus() {
	if (!SendATCommand(std::string("AT+CPIN?").c_str(),	true, (char*)"+CPIN"))      ///< Check if SIM is inserted and if PIN required
	{
		return false;
	}
	return true;
}

bool GSMModem::GsmGetNetworkRegistrationSIMStaus(bool *status, char *networkProvider) {
	size_t bytesTransmitted = 0;
	size_t noOfBytesRead = 0;
	char buffer[100] = {0};
	char *start = NULL;
	char *end = NULL;

	*status = SendATCommand(std::string("AT+CREG?").c_str(), true, 50, buffer);   ///< Check registration status
	if (*status && (strstr(buffer, "+CREG: 0,1") || strstr(buffer, "+CREG: 0,5"))) {
		memset(buffer, 0, 100);
		bytesTransmitted = handle->write(std::string("AT+COPS?\r\n"));
		if (bytesTransmitted != 10)
			return false;

		usleep(250000);
		noOfBytesRead = handle->read((unsigned char*)buffer, 100);
		if (noOfBytesRead == 0)
			return false;

		start = strstr(buffer, "\"");
		end = strstr(start + 1, "\"");
		if (!start || !end)
			return false;
		strncat(networkProvider, start, end - start + 1);
		return true;
	} else
		return false;
}

bool GSMModem::GsmGetNetworkDateTimeInformation(char *date, char *time) {
	size_t bytesTransmitted = 0;
	size_t noOfBytesRead = 0;
	char buffer[100] = {0};
	char *start = NULL;
	char *end = NULL;

	bytesTransmitted = handle->write(std::string("AT+CCLK?\r\n"));
	if (bytesTransmitted != 10)
		return false;
	usleep(250000);
	noOfBytesRead = handle->read((unsigned char*)buffer, 100);
	if (noOfBytesRead == 0)
		return false;
	start = strstr(buffer, "\"");
	end = strstr(start + 1, ",");
	if (!start || !end)
		return false;
	strncat(date, start + 1, end - (start + 1));

	start = strstr(buffer, ",");
	end = strstr(start + 1, "\"");
	if (!start || !end)
		return false;
	strncat(time, start + 1, end - (start + 1));
	return true;
}

bool GSMModem::GsmSendSms(char *phoneNumber, char *message) {
	size_t bytesTransmitted = 0;
	char buffer[1024] = {0};
	memset(buffer, 0, 1024);

	if (phoneNumber == NULL || strlen(phoneNumber) < 13 || message == NULL || strlen(message) > 160)
		return false;
	std::string cmd("AT+CMGS=\"");
	cmd += phoneNumber;
	cmd += "\"";
	if (!SendATCommand(cmd.c_str(), true, (char*)">"))
		return false;

	bytesTransmitted = handle->write((unsigned char*)message, strlen(message));
	if (bytesTransmitted != strlen(message))
		return false;

	return SendATCommand(std::string(1, 26).c_str(), false, 20, buffer);
}

bool GSMModem::GsmRecieveSms(char *messageIndex, char *senderPhoneNumber, char *dateTimeStamp, char *message, size_t messageSize) {
	char readBuffer[512] = {0};
	char *start = NULL;
	char *end = NULL;
	size_t totalReadBytes = 0, tmp;

	std::string cmd("AT+CMGR=");
	cmd += static_cast<std::ostringstream &>(( std::ostringstream() << std::dec << messageIndex )).str();

	SendATCommand(cmd.c_str(), true, NULL);

	totalReadBytes = handle->read((unsigned char*)readBuffer, 512);
	sleep(1);
	tmp = handle->read((unsigned char*)readBuffer, 512);
	if (tmp > 0)
		totalReadBytes = tmp;
	if (totalReadBytes < 1)
		return false;
	start = strstr(readBuffer, ",");
	if (!start)
		return false;
	end = strstr(start + 1, ",");
	if (!strstr(readBuffer, "+CMGR: ") || !strstr(readBuffer, "+") || !end)
		return false;

	strncat(senderPhoneNumber, start + 2, end - start - 3);

	start = strstr(end + 1, ",");
	if (!start)
		return false;
	end = strstr(start + 2, "\"");
	if (!end)
		return false;
	strncat(dateTimeStamp, start + 2, end - start - 2);

	strncat(message, end + 3, strlen(readBuffer) - (end - readBuffer) - 11);
	return true;
}

bool GSMModem::GsmDeleteSms(char *messageIndex) {
	std::string cmd("AT+CMGD=");
	cmd += static_cast<std::ostringstream &>(( std::ostringstream() << std::dec << messageIndex )).str();
	return SendATCommand(cmd.c_str(), true, NULL);
}

bool GSMModem::GsmInitializeSms() {
	if (GsmGetSIMStaus()) {
		if (!SendATCommand(std::string("AT+CMGF=1").c_str(),	true, (char*)"OK"))      ///< Check if SIM is inserted and if PIN required
		{
			return false;
		}
		return true;
	}
	return false;
}

bool GSMModem::GsmGetSmsList(std::string &smslist) {
	char tmp[65536];
	std::string cmd("AT+CMGL=\"ALL\"");
	if (SendATCommand(cmd.c_str(), true, 50, tmp)) {
		smslist = std::string(tmp);
		return true;
	}
	return false;
}

int GSMModem::GsmReadI2CReg(uint8_t address, uint8_t reg) {
	int ret = -1;
	char buffer[100] = {0};
	char cmd[25] = {0};
	char *start;
	sprintf(cmd, "AT#I2CRD=9,10,%02X,%02X,1", address << 1, reg);

	memset(buffer, 0, 100);

	if (SendATCommand(cmd, true, 5, buffer) && (start = strstr(buffer, "#I2CRD:"))) {
		std::stringstream ss;
		ss << std::hex << (start + 8);
		ss >> ret;
		std::cout << start << std::endl;
	}
	return ret;
}

bool GSMModem::GsmWriteI2CReg(uint8_t address, uint8_t reg, uint8_t value) {
	char cmd[30] = {0};
	//size_t bytesTransmitted = 0;
	char buffer[1024] = {0};
	memset(buffer, 0, 1024);

	sprintf(cmd, "AT#I2CWR=9,10,%02X,%02X,1", address << 1, reg);
	std::cout << cmd << std::endl;
	if (!SendATCommand(cmd, true, (char*)">"))
		return false;
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "%02X%s", value, std::string(1, 26).c_str());
	//bytesTransmitted = handle->write((unsigned char*)&value, 2);
	//if (bytesTransmitted != 2)
	//	return false;
	return SendATCommand(cmd, true, (char*)"OK");
}

std::string GSMModem::ExecuteShellCmd(std::string cmd) {
	char buffer[128];
	std::string result = "";
	FILE* pipe = popen(cmd.c_str(), "r");
	if (!pipe) throw std::runtime_error("popen() failed!");
	try {
		while (!feof(pipe)) {
			if (fgets(buffer, 128, pipe) != NULL)
				result += buffer;
		}
	} catch (...) {
		pclose(pipe);
		throw;
	}
	pclose(pipe);
	return result;
}

} /* namespace RFGSM */
