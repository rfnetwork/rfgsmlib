/*
 * GSMModem.h
 *
 *  Created on: Nov 14, 2016
 *      Author: ubuntu
 */

#ifndef GSMMODEM_H_
#define GSMMODEM_H_

#include <string.h>
#include <unistd.h>
#include "serial.h"
#include <iostream>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <vector>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cstdlib>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

#define DELIVERY_REPORT_SMS_SENT        0
#define DELIVERY_REPORT_SMS_DELIVERED   1
#define IMEI_NUMBER_MAX_LENGTH          15
#define DATE_MAX_LENGTH                 8
#define TIME_MAX_LENGTH                 11

namespace RFGSM {

class GSMModem {
public:
	GSMModem();
	virtual ~GSMModem();
	/*
	 * Configure COM port to communicate with GSM modem
	 */
	bool GSMOpen();
	/*
	 * Close COM port used for GSM modem
	 */
	bool GSMClose();
	/*
	 * Check if modem exists (if all ttyACM devices exist)
	 */
	bool GSMModemExists();
	/*
	 * Gets IMEI number of the modem
	 */
	bool GSMGetIMEINumber(char *imeiNumber);
	/*
	 * Gets signal strength information
	 *
	 * @signalStrength Signal strength information
	 */
	bool GsmGetSignalStrength(char *signalStrength);
	/*
	 * Gets signal strength information
	 *
	 * @signalStrength Signal strength information in dBm
	 */
	bool GsmGetSignalStrengthDMB(int *signalStrength);
	/*
	 * Gets information about modem manufacturer
	 *
	 * @manufacturer   Manufacturer information
	 * @modelNumber    Model number of modem
	 */
	bool GsmGetManufacturerInfo(char *manufacturer, char *modelNumber);
	/*
	 * Gets information about software version
	 *
	 * @version   Software version
	 */
	bool GsmGetSWVersion(char *version);
	/*
	 * Gets SIM pin status
	 *
	 * @status SIM status
	 */
	bool GsmGetSIMStaus();
	/*
	 * Gets information of SIM registration and network provider
	 *
	 * @status 			Status of SIM registration
	 * @networkProvider Network provider information
	 */
	bool GsmGetNetworkRegistrationSIMStaus(bool *status, char *networkProvider);
	/*
	 * Gets Network Date and Time Information
	 *
	 * @date Pointer to date string
	 * @time Pointer to time string
	 */
	bool GsmGetNetworkDateTimeInformation(char *date, char *time);
	/*
	 * Send SMS to a phone number
	 *
	 * @phoneNumber       Phone number to which SMS is send
	 * @message           Message to send in SMS
	 */
	bool GsmSendSms(char *phoneNumber, char *message);
	/*
	 * Read received SMS
	 *
	 * @messageIndex         Index of received SMS
	 * @senderPhoneNumber    SMS sender phone number
	 * @dateTimeStamp        Date and time of received SMS
	 * @message              Message received in SMS
	 * @messageSize          Message size
	 */
	bool GsmRecieveSms(char *messageIndex, char *senderPhoneNumber, char *dateTimeStamp, char *message, size_t messageSize);
	/*
	 * Delete SMS
	 *
	 * @messageIndex   Index of SMS to be deleted
	 */
	bool GsmDeleteSms(char *messageIndex);

	/*
	 * Initialize SMS functionality
	 */
	bool GsmInitializeSms();

	/*
	 * Get list of all SMS
	 *
	 * @smslist				List of SMS according +CMGL at command
	 */
	bool GsmGetSmsList(std::string &smslist);

	int GsmReadI2CReg(uint8_t address, uint8_t reg);
	bool GsmWriteI2CReg(uint8_t address, uint8_t reg, uint8_t value);
private:
	/*
	 * Send AT command to GSM modem
	 *
	 * @commandString AT command to be send
	 * @terminate End of message
	 * @responseString Expected AT command response
	 */
	bool SendATCommand(const char *commandString, bool terminate, char *responseString);

	/*
	 * Send AT command to GSM modem
	 *
	 * @commandString AT command to be send
	 * @terminate End of message
	 * @timeout Read timeout
	 * @responseString Expected AT command response
	 */
	bool SendATCommand(const char *commandString, bool terminate, int timeout, char *responseString);
	serial::Serial *handle;

	std::string ExecuteShellCmd(std::string cmd);
};

} /* namespace RFGSM */

#endif /* GSMMODEM_H_ */
